function(values) {
    var result = {};
    for(var i in values) {
        for(var k in values[i]) {
            if(k in result) result[k] += values[i][k];
            else result[k] = values[i][k];			
        }
    }
    return [result];
}
