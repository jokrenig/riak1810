# usage: push_json.py [-h] [--host HOST] [-p PORT] -b BUCKET -k KEYFIELD file

import riak, argparse, json

def get_args():
    parser = argparse.ArgumentParser()
    parser.add_argument('--host', help='host', default='127.0.0.1')
    parser.add_argument('-p', '--port', help='http port', default=10018, type=int)
    parser.add_argument('-b', '--bucket', required=True, help='bucket')
    parser.add_argument('-k', '--keyfield', required=True, help='key field')
    parser.add_argument('file', help='json file')
    return parser.parse_args()

def push(data):
    client = riak.RiakClient(protocol='http', host=data.host, http_port=data.port)
    assert client.ping()
    bucket = client.bucket(data.bucket)
    with open(data.file, mode='r') as file:
        for record in json.load(file):
            key = record[data.keyfield]
            try:
                ro = bucket.new(key, data=record, content_type='application/json')
                ro.store()
                print(ro.data)
            except:
                print('Error', key)

if __name__ == '__main__':
    push(get_args())
