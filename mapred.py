# usage: mapred.py [-h] [--host HOST] [-p PORT] -b BUCKET mapfunc [redfunc]

import riak, argparse, json

def get_args():
    parser = argparse.ArgumentParser()
    parser.add_argument('--host', help='host', default='127.0.0.1')
    parser.add_argument('-p', '--port', help='http port', default=10018, type=int)
    parser.add_argument('-i', '--input', required=True, help='input for map-reduce (a bucket name)')
    parser.add_argument('-m', '--map', required=True, help='map function file')
    parser.add_argument('-r', '--reduce', nargs='?', help='reduce function file')
    return parser.parse_args()

def get_query(name, source_file):
    source = ''
    with open(source_file, mode='r') as file:
        source = file.read()
    return {'language': 'javascript', 'source': source}

def mapred(data):
    query = [{'map': get_query('map', data.map)}]
    if data.reduce:
        query.append({'reduce': get_query('reduce', data.reduce)})
    print(json.dumps(query, indent=4, separators=(',', ': ')))
    client = riak.RiakClient(protocol='http', host=data.host, http_port=data.port)
    assert client.ping()
    return client.mapred(data.input, query, 10000)	

if __name__ == '__main__':
    print(mapred(get_args()))


